#!/usr/bin/env bash

exec 4>&1; export BASH_XTRACEFD=4; set -x

# The URL of the third party CEF package we want to extract from and build. This can either
# be a link to the Standard Distribution on the Spotify CEF Automated Builds page here:
# http://opensource.spotify.com/cefbuilds/index.html. These builds are also mirrored into
# a Linden Lab AWS bucket for posterity and so that can also be used.
#CEF_ARCHIVE="https://bitbucket.org/Drakeo/cef/downloads/cef_binary_3.3626.1895.g7001d56_linux64_minimal.tar.bz2"
#CEF_ARCHIVE="https://bitbucket.org/Drakeo/cef/downloads/cef_binary_81.3.3g072a5f5chromium-81.0.4044.138_linux64.tar.bz2"
CEF_ARCHIVE="https://bitbucket.org/Drakeo/cef/downloads/cef_binary_81.3.10gb223419chromium-81.0.4044.138_linux64.tar.bz2"

# The version string you want to use (also used as the directory name for the final build result).  This
# version information used to be used to construct the URL directly but the version numbers are now so
# complex that this is infeasible - you can use any name you like here but it's sensible to base it on
# the contents of the URL - perhaps with a tag to indicate if media codecs are enabled for example.
CEF_BUILD="81.3.10.spot"

# Build in the system tmp directory so we can store and process intermediate results
DST_DIR="$(pwd)/cef_builds/"
SRC_DIR="$(pwd)/linuxsrc"
CEF_DIR="cef_${CEF_BUILD}"

mkdir -p "${SRC_DIR}/${CEF_DIR}"
mkdir -p "${DST_DIR}/${CEF_DIR}"

pushd .

cd ${SRC_DIR}

# download the archive
wget -c "$CEF_ARCHIVE" -O "${CEF_DIR}.tar.bz2"

# extract into the named directory nad strip off the top level as these builds have
# a long descriptive top level directory we do not need and cannot interrogate
tar -xvf "${CEF_DIR}.tar.bz2" --directory "${CEF_DIR}" --strip 1

cd "${CEF_DIR}"

# generate project files
mkdir build64
cd build64
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Rlease ..

# build release and debug configurations // drakeo only release
make -j6 libcef_dll_wrapper

# copy libraries
mkdir -p "${DST_DIR}/${CEF_DIR}/lib/release"
cp -R ${SRC_DIR}/${CEF_DIR}/Release/*  ${DST_DIR}/${CEF_DIR}/lib/release/



# copy headers (remember, the layout Dullahan expects matches the Linden 3p CEF autobuild one with an
# extra level of nexting in cef/include/include vs the standard CEF include location)
mkdir -p "${DST_DIR}/${CEF_DIR}/include/cef/include"
cp -R ${SRC_DIR}/${CEF_DIR}/include/*  ${DST_DIR}/${CEF_DIR}/include/

# copy libcef_dll library
mkdir -p "${DST_DIR}/${CEF_DIR}/lib/release"
cp ${SRC_DIR}/${CEF_DIR}/build64/libcef_dll_wrapper/libcef_dll_wrapper.a ${DST_DIR}/${CEF_DIR}/lib/release/

# copy resourses
mkdir -p "${DST_DIR}/${CEF_DIR}/resources"
cp -R ${SRC_DIR}/${CEF_DIR}/Resources/* ${DST_DIR}/${CEF_DIR}/resources/

# copy LICENSE.txt
mkdir -p "${DST_DIR}/${CEF_DIR}/LICENSES"
cp ${SRC_DIR}/${CEF_DIR}/LICENSE.txt ${DST_DIR}/${CEF_DIR}/LICENSES/cef.txt
# move it up top
mkdir -p ${SRC_DIR}/../../stage
mv ${DST_DIR}/${CEF_DIR} ${SRC_DIR}/../../stage/packages
popd
