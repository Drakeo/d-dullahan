#!/bin/sh
CWD="$(pwd)/build_cef"
# create work area
if [ -d build_cef ]; then
    cd $CWD	
else
	mkdir -p $CWD 
	cd $CWD	
fi

echo -e "\e[1;33m Pause 5 seconds to read this!!!! First time building this will take 10 to 15 HOURS!!!!! hours\e[0m"
echo -e "\e[1;34m Part of that is downloading Chromium\e[0m"
sleep 5
# need the latest automate-git.py for  builds 4044 to 
wget -c https://bitbucket.org/chromiumembedded/cef/raw/b2b49f1076a0bd5bc4951badd8b2fdfbfd45c1c9/tools/automate/automate-git.py
# Need build tools 
if [ -d $CWD/depot_tools ]; then
echo -e "\e[1;33m depot_tools already here you may want to update them sometime.\e[0m"
sleep 2
else
	git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git 
fi
# set path
export PATH=$CWD/depot_tools:$PATH
# push configuration to chromium GN
export GN_DEFINES="is_official_build=true  symbol_level=1 \
enable_hangout_services_extension=true \
enable_pepper_cdms=true \
enable_precompiled_headers=false \
enable_swiftshader=true \
enable_webrtc=true \ #turned off with LLceflib build 
enable_widevine=true \
enable_platform_hevc=true \
enable_platform_mpeg_h_audio=true \
enable_platform_dolby_vision=true \
fatal_linker_warnings=false \
ffmpeg_branding=Chrome \ # this allows VLC to be dropped no need for it VLC is ffmpeg
linux_strip_binary=true \
linux_use_bundled_binutils=false \
proprietary_codecs=true \ # same with vlc and ffmpeg
remove_webcore_debug_symbols=true \
treat_warnings_as_errors=false \
use_allocator=none \
use_bundled_fontconfig=false \
use_cfi_icall=false \
use_cups=true \
use_custom_libcxx=true \
use_gconf=false \
use_kerberos=false \
use_pulseaudio=true \
use_system_libdrm=false \
use_system_minigbm=false \
use_sysroot=false \
use_jumbo_build=true"

# use lz bz2 for packaging 
export CEF_ARCHIVE_FORMAT=tar.bz2   
# commands for automate-git.py
python automate-git.py --download-dir=$CWD/chromium_git --depot-tools-dir=$CWD/depot_tools --force-clean  --branch=4044 --x64-build --no-debug-build 

# below quick quick comands to replace above if errors.
#ubuntu may have to use newer ATK slackware 14.2 must upgrade ATK
# --force-clean-deps # when doing a complete new build version
# --force-clean # when you just want to clean your build
# --force-build # force a new config but start the build must remove --force-clean --force-clean-deps from command line.
# --no-update # used when you need to upgrade a library used with --force-build
# use_system_libdrm=false \ drm fail? must be used with use_system_minigbm=false use slackware 14.2 ubuntu 16.04 Ubuntu 17? 18 ? and older
# use_system_minigbm=false \gbm fail ? must be used with use_system_libdrm=false use slackware 14.2 ubuntu 16.04 Ubuntu 17? 18 ? and older 
# use_jumbo_build=true" can be used with 4044 with this cut time in half 
# https://bitbucket.org/chromiumembedded/cef/raw/b2b49f1076a0bd5bc4951badd8b2fdfbfd45c1c9/tools/automate/automate-git.py  
